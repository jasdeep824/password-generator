const charSet = {
  lowerCase: "abcdefghijklmnopqrstuvwxyz",
  upperCase: "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
  numerical: "0123456789",
  specialCase: "!@#$%^&*()_+{}[]|:;<>,.?/~",
};

export default charSet;
