import React, { useState } from "react";

import CoreButton from "../components/CoreButton";
import CoreCheckbox from "../components/CoreCheckbox";
import CoreSlider from "../components/CoreSlider";
import TextInput from "../components/TextInput";

import checkPasswordStrength from "../helper/password-helper/checkStrength";
import copyToClipboard from "../helper/password-helper/copyToClipboard";
import generateRandomPassword from "../helper/password-helper/passwordGenerator";
import IPasswordOptions from "../interface/IPasswordOption";

import ContentPasteIcon from "@mui/icons-material/ContentPaste";
import ReplayIcon from "@mui/icons-material/Replay";

import IconGif from "./../assets/icon.gif";

import { Box, Container } from "@mui/material";
import getPasswordStrengthColor from "../helper/password-helper/getPasswordStrengthColor";
import areOptionsEnabled from "../helper/password-helper/areOptionsEnabled";

const PasswordGenerator: React.FC = () => {
  const [password, setPassword] = useState<string>("");
  const [copied, setCopied] = useState<boolean>(false);
  const [passwordStrength, setPasswordStrength] = useState<string>("");
  const [passwordOptions, setPasswordOptions] = useState<IPasswordOptions>({
    length: 10,
    includeLowercase: true,
    includeUppercase: true,
    includeNumbers: true,
    includeSpecialChars: true,
    excludeChars: "",
  });

  const handleGeneratePassword = () => {
    const newPassword = generateRandomPassword(passwordOptions);
    setPassword(newPassword);

    const strength = checkPasswordStrength(newPassword);
    setPasswordStrength(strength);
  };

  const handleReloadPassword = () => {
    setCopied(false);
    handleGeneratePassword();
  };

  const handleCopyToClipboard = () => {
    copyToClipboard(password);
    setCopied(true);
  };

  const handleOptionChange = (
    key: keyof IPasswordOptions,
    value: boolean | number | string
  ) => {
    setCopied(false);
    setPasswordOptions((prevOptions) => ({
      ...prevOptions,
      [key]: value,
    }));
  };

  return (
    <Container
      sx={{
        height: "100vh",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Box
        sx={{
          display: "flex",
          borderRadius: "20px",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          textAlign: "center",
          width: "65%",
          height: "95%",
          border: "1px solid #588b54",
          background: "#FFFFFF",
        }}
      >
        <img src={IconGif} width={75} alt="lock gif" />
        <h2>PASSWORD GENERATOR</h2>
        <p>Create strong and secure passwords</p>

        {password && (
          <div style={{ border: "1px solid black", width: "280px" }}>
            <p>{password}</p>
          </div>
        )}
        <p style={{ color: getPasswordStrengthColor(passwordStrength) }}>
          {passwordStrength}
        </p>
        <Box
          sx={{
            display: "flex",
            flexDirection: "row",
            gap: "10px",
          }}
        >
          <CoreButton
            icon={<ReplayIcon />}
            text="Generate"
            onClick={handleReloadPassword}
            disabled={!areOptionsEnabled(passwordOptions)}
          />
          <CoreButton
            icon={<ContentPasteIcon />}
            text={copied ? "Copied" : "Copy"}
            onClick={handleCopyToClipboard}
            disabled={!password}
          />
        </Box>

        <Box
          sx={{ display: "flex", paddingTop: "10px", flexDirection: "column" }}
        >
          <CoreSlider
            value={passwordOptions.length}
            min={4}
            max={30}
            onChange={(value) => handleOptionChange("length", value)}
          />
          <CoreCheckbox
            checked={passwordOptions.includeLowercase}
            onChange={(checked) =>
              handleOptionChange("includeLowercase", checked)
            }
            label="Include Lowercase"
          />
          <CoreCheckbox
            checked={passwordOptions.includeUppercase}
            onChange={(checked) =>
              handleOptionChange("includeUppercase", checked)
            }
            label="Include Uppercase"
          />
          <CoreCheckbox
            checked={passwordOptions.includeNumbers}
            onChange={(checked) =>
              handleOptionChange("includeNumbers", checked)
            }
            label="Include Numbers"
          />
          <CoreCheckbox
            checked={passwordOptions.includeSpecialChars}
            onChange={(checked) =>
              handleOptionChange("includeSpecialChars", checked)
            }
            label="Include Special Characters"
          />
          <TextInput
            placeholder="Exclude Characters"
            value={passwordOptions.excludeChars}
            onChange={(value) => handleOptionChange("excludeChars", value)}
          />
        </Box>
      </Box>
    </Container>
  );
};

export default PasswordGenerator;
