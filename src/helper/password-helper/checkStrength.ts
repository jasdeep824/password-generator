import { passwordStrength } from "check-password-strength";

const checkPasswordStrength = (password: string): string => {
  return passwordStrength(password).value;
};

export default checkPasswordStrength;
