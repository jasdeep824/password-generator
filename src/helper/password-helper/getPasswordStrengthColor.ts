const getPasswordStrengthColor = (strength: string): string => {
  switch (strength) {
    case "Weak":
    case "Too weak":
      return "red";
    case "Medium":
      return "orange";
    case "Strong":
      return "green";
    default:
      return "";
  }
};

export default getPasswordStrengthColor;
