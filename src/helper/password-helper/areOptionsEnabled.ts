const areOptionsEnabled = (passwordOptions: {
  includeLowercase: boolean;
  includeUppercase: boolean;
  includeNumbers: boolean;
  includeSpecialChars: boolean;
}): boolean => {
  return (
    passwordOptions.includeLowercase ||
    passwordOptions.includeUppercase ||
    passwordOptions.includeNumbers ||
    passwordOptions.includeSpecialChars
  );
};

export default areOptionsEnabled;
