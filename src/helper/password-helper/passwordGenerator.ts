import charSet from "../../enum/characters";
import IPasswordOptions from "../../interface/IPasswordOption";

const generateRandomPassword = ({
  length,
  includeLowercase,
  includeUppercase,
  includeNumbers,
  includeSpecialChars,
  excludeChars,
}: IPasswordOptions): string => {
  let charset = "";
  if (includeLowercase) charset += charSet.lowerCase;
  if (includeUppercase) charset += charSet.upperCase;
  if (includeNumbers) charset += charSet.numerical;
  if (includeSpecialChars) charset += charSet.specialCase;

  // if no option is enabled, return empty string
  if (!charset) return "";

  // excluded characters
  charset = charset.replace(new RegExp(`[${excludeChars}]`, "g"), "");

  let newPassword = "";
  for (let i = 0; i < length; i++) {
    const randomIndex = Math.floor(Math.random() * charset.length);
    newPassword += charset[randomIndex];
  }
  return newPassword;
};

export default generateRandomPassword;
