const copyToClipboard = (text: string): void => {
  navigator.clipboard.writeText(text);
};

export default copyToClipboard;
