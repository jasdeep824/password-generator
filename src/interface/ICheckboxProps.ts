interface ICheckboxProps {
  label: string;
  checked: boolean;
  onChange: (checked: boolean) => void;
}

export default ICheckboxProps;
