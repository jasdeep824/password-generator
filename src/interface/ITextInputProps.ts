interface ITextInputProps {
  value: string;
  placeholder: string;
  onChange: (value: string) => void;
}

export default ITextInputProps;
