interface ISliderProps {
  value: number;
  onChange: (value: number) => void;
  min?: number;
  max?: number;
  step?: number;
  leftMark?: string;
  rightMark?: string;
}

export default ISliderProps;
