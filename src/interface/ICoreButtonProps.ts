import { ButtonOwnProps } from "@mui/material";
import { ReactNode } from "react";

interface ICoreButtonProps extends ButtonOwnProps {
  text: string;
  icon: ReactNode;
  disabled: boolean;
  onClick?: () => void;
}

export default ICoreButtonProps;
