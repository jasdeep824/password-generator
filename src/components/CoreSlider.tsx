import React from "react";
import { Slider as MuiSlider } from "@mui/material";
import ISliderProps from "../interface/ISliderProps";

const CoreSlider: React.FC<ISliderProps> = ({
  value,
  onChange,
  min = 0,
  max = 100,
  step = 1,
  leftMark = min,
  rightMark = max,
}) => {
  const handleChange = (event: Event, newValue: number | number[]) => {
    if (typeof newValue === "number") {
      onChange(newValue);
    }
  };

  return (
    <MuiSlider
      value={value}
      onChange={handleChange}
      min={min}
      max={max}
      marks={[
        { value: min, label: leftMark },
        { value: max, label: rightMark },
      ]}
      step={step}
      valueLabelDisplay="auto"
    />
  );
};

export default CoreSlider;
