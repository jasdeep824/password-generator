import Button from "@mui/material/Button";
import ICoreButtonProps from "../interface/ICoreButtonProps";

const CoreButton: React.FC<ICoreButtonProps> = ({
  icon,
  size,
  text,
  disabled,
  onClick,
}) => {
  return (
    <Button
      disabled={disabled}
      variant="contained"
      size={size}
      startIcon={icon}
      onClick={onClick}
    >
      {text}
    </Button>
  );
};

export default CoreButton;
