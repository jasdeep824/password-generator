import React from "react";
import { Checkbox, FormControlLabel } from "@mui/material";
import ICheckboxProps from "../interface/ICheckboxProps";

const CoreCheckbox: React.FC<ICheckboxProps> = ({
  label,
  checked,
  onChange,
}) => {
  return (
    <FormControlLabel
      control={
        <Checkbox
          checked={checked}
          onChange={(event) => onChange(event.target.checked)}
        />
      }
      label={label}
    />
  );
};

export default CoreCheckbox;
