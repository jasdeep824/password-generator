import React from "react";
import TextField from "@mui/material/TextField";
import ITextInputProps from "../interface/ITextInputProps";

const TextInput: React.FC<ITextInputProps> = ({
  placeholder,
  value,
  onChange,
}) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    onChange(event.target.value);
  };

  return (
    <TextField
      variant="outlined"
      placeholder={placeholder}
      value={value}
      onChange={handleChange}
      InputLabelProps={{ shrink: false }}
    />
  );
};

export default TextInput;
