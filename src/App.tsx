import "./App.css";
import PasswordGenerator from "./pages/PasswordGenerator";

const App = () => {
  return (
    <div className="App">
      <PasswordGenerator />
    </div>
  );
};

export default App;
